package ictgradschool.web.lab09.ex02;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class ArticleServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html");

        // request.getParameter accesses parameters from the submitted form
        String atitle = request.getParameter("atitle");
        String aauthor= request.getParameter("aauthor");
        String content= request.getParameter("content");
        String genre= request.getParameter("genre");
        // printWriter is used to write to the HTML document
        PrintWriter out = response.getWriter();


        out.print("<h2>"+atitle+"</h2>\n");
        out.println("<p>by " + aauthor+"<br>");
        out.print("Genre:"+ genre +"</p>");
        out.print(content);

        String[] numbers= new String[4];
        numbers[0] = "Four";
        numbers[1] = "Three";
        numbers[2] = "Two";
        numbers[3] = "One";

       out.println("<p>This is a string array from Java: </p>");
       out.println("<ul>");
       for (String n: numbers){
           out.println("<li>" + n + "</li>");
       }

       out.println("</ul>");

       out.println("<body>\n<html>");



    }

     protected void dopost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        doGet(request,response);
     }

}

