package ictgradschool.web.lab09.ex07;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

public class ImageGalleryDisplay extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        doGet(request, response);

    }


        protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


        response.setContentType("text/html");

        PrintWriter out = response.getWriter();

            ServletContext servletContext = getServletContext();
            String fullPhotoPath = servletContext. getRealPath("/Photos");
            File myFolder = new File(fullPhotoPath);
            String[] listName = myFolder.list();

            for( int i=0; i< listName.length; i++) {

                String[] str = listName[i].split("_");

                for (int j = 0; j< str.length; j++){
                    if(str[j].equals("thumbnail.png")) {
                        String s = "";
                        switch(j){
                            case 1:
                                s= str[j-1];
                                break;
                            case 2:
                                s= str[j-2] + "_"+ str[j-1];
                                break;
                            case 3:
                                s = str[j-3] + "_" + str[j-2] + "_" + str[j-1];
                                break;
                        }


                        out.println("<a href =\"../Photos/" + s + ".jpg\">" + " <img src=\"../Photos/" + listName[i] + "\"></a>");
                        out.println();

                        out.println("<p>" + s + "</p>");

                        File photo = new File ( fullPhotoPath + "/" + s + ".jpg");



                        out.println("<p>" + "Original Size:" + photo.length() + "</p>");



                    }
                }


            }







    }



//    ServletContext servletContext = getServletContext();
//    String fullPhotoPath = servletContext.getRealPath("/Photos");
//
//     protected void dopost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
//        doGet(request,response);
//     }

}

